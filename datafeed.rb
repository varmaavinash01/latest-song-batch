require 'rest-client'
require 'nokogiri'
require 'open-uri'

#RestClient.proxy = "http://dev-proxy.db.rakuten.co.jp:9501/"

urls = [
  "http://www.billboard.com/charts/hot-100",
  "http://www.billboard.com/charts/hot-100?page=1",
  "http://www.billboard.com/charts/hot-100?page=2",
  "http://www.billboard.com/charts/hot-100?page=3",
  "http://www.billboard.com/charts/hot-100?page=4"
  ]

songs = []

urls.each do |url|
  #contents = RestClient.get url
  doc = Nokogiri::HTML(open(url))
  articles = []
  week = doc.xpath("//*[contains(@class, 'chart_date')]").first.text.gsub(",","").gsub(" ", "-")
  p week	 
  doc.xpath("//article").each do |a|
    if a.get_attribute("class").split(" ").include? "song_review"
      song = {}
      a.xpath(".//h1").each do |h|
        song["name"] = h.children.text.strip
      end
      a.xpath(".//p").each do |p|
        if p.get_attribute("class") == "chart_info"
          song["artist"] = p.xpath(".//a").first.children.text.strip
        end
      end
      song["rank"] = a.xpath(".//span").first.children.text
      songs.push song
    end
  end
end

p songs
p songs.size
